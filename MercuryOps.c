#include "MercuryOps.h"

MercuryOperation MercuryOperations[] = {
    {MercuryNoOperation,                MercuryNone},
    {MercuryPushAddress,                MercuryImmediate},
    {MercuryPushImmediate,              MercuryImmediate},
    {MercuryPopOp,                      MercuryNone},
    {MercuryPopOp,                      MercuryImmediate},
    {MercuryPickOp,                     MercuryNone},
    {MercuryPickOp,                     MercuryImmediate},
    {MercuryRollOp,                     MercuryNone},
    {MercuryRollOp,                     MercuryImmediate},
    {MercuryDuplicate,                  MercuryNone},
    {MercuryDrop,                       MercuryNone},
    {MercurySwap,                       MercuryNone},
    {MercuryRotate,                     MercuryNone},
    {MercuryAdd,                        MercuryNone},
    {MercuryAdd,                        MercuryImmediate},
    {MercuryIncrement,                  MercuryNone},
    {MercurySubtract,                   MercuryNone},
    {MercurySubtract,                   MercuryImmediate},
    {MercuryDecrement,                  MercuryNone},
    {MercuryMultiply,                   MercuryNone},
    {MercuryMultiply,                   MercuryImmediate},
    {MercuryDivide,                     MercuryNone},
    {MercuryDivide,                     MercuryImmediate},
    {MercuryModulo,                     MercuryNone},
    {MercuryModulo,                     MercuryImmediate},
    {MercuryLogicalAND,                 MercuryNone},
    {MercuryLogicalAND,                 MercuryImmediate},
    {MercuryLogicalOR,                  MercuryNone},
    {MercuryLogicalOR,                  MercuryImmediate},
    {MercuryLogicalNOT,                 MercuryNone},
    {MercuryLogicalNOT,                 MercuryImmediate},
    {MercuryLogicalXOR,                 MercuryNone},
    {MercuryLogicalXOR,                 MercuryImmediate},
    {MercuryBitwiseAND,                 MercuryNone},
    {MercuryBitwiseAND,                 MercuryImmediate},
    {MercuryBitwiseOR,                  MercuryNone},
    {MercuryBitwiseOR,                  MercuryImmediate},
    {MercuryBitwiseNOT,                 MercuryNone},
    {MercuryBitwiseNOT,                 MercuryImmediate},
    {MercuryBitwiseXOR,                 MercuryNone},
    {MercuryBitwiseXOR,                 MercuryImmediate},
    {MercuryShiftLeft,                  MercuryNone},
    {MercuryShiftLeft,                  MercuryImmediate},
    {MercuryShiftRight,                 MercuryNone},
    {MercuryShiftRight,                 MercuryImmediate},
    {MercuryCompare,                    MercuryNone},
    {MercuryCompare,                    MercuryImmediate},
    {MercuryBranchLessThan,             MercuryBranch},
    {MercuryBranchLessThan,             MercuryImmediate | MercuryBranch},
    {MercuryBranchLessThanOrEqualTo,    MercuryBranch},
    {MercuryBranchLessThanOrEqualTo,    MercuryImmediate | MercuryBranch},
    {MercuryBranchEqualTo,              MercuryBranch},
    {MercuryBranchEqualTo,              MercuryImmediate | MercuryBranch},
    {MercuryBranchGreaterThan,          MercuryBranch},
    {MercuryBranchGreaterThan,          MercuryImmediate | MercuryBranch},
    {MercuryBranchGreaterThanOrEqualTo, MercuryBranch},
    {MercuryBranchGreaterThanOrEqualTo, MercuryImmediate | MercuryBranch},
    {MercuryJump,                       MercuryModifyProgramCounter},
    {MercuryJump,                       MercuryImmediate |
                                               MercuryModifyProgramCounter},
    {MercuryJumpSubroutine,             MercuryModifyProgramCounter},
    {MercuryJumpSubroutine,             MercuryImmediate |
                                               MercuryModifyProgramCounter},
    {MercuryReturnSubroutine,           MercuryModifyProgramCounter},
    {MercuryCallModule,                 MercuryModifyProgramCounter},
    {MercuryCallModule,                 MercuryImmediate |
                                               MercuryModifyProgramCounter},
    {MercuryHalt,                       MercuryNone}
};

MercuryBool MercuryPush(MercuryVM* vm, MercuryUnsigned data) {
    if(vm == MercuryNull) {
        return false;
    }
    else if(vm->StackTop == vm->StackSize) {
        vm->Status = MercuryStackOverflow;
        return false;
    }
    else {
        vm->Stack[vm->StackTop] = data;
        vm->StackTop++;
        vm->Status              = MercuryNone;
        return true;
    }
}

MercurySigned MercuryPop(MercuryVM* vm) {
    MercurySigned operand;
    if(vm == MercuryNull) {
        return false;
    }
    else if(vm->StackTop == 0) {
        vm->Status = MercuryStackUnderflow;
        return false;
    }
    else {
        vm->StackTop--;
        operand                 = vm->Stack[vm->StackTop];
        vm->Stack[vm->StackTop] = 0;
        vm->Status              = MercuryNone;
        return operand;
    }
}

MercuryBool MercuryPick(MercuryVM* vm, MercuryUnsigned data) {
    if(vm->StackTop == 0 ||
       (vm->StackTop - 1) < data) {
        vm->Status  = MercuryStackUnderflow;
        return false;
    }
    else {
        vm->Status = MercuryNone;
        return MercuryPush(vm, vm->Stack[(vm->StackTop - 1) - data]);
    }
}

MercuryBool MercuryRoll(MercuryVM* vm, MercuryUnsigned data) {
    /*Not done!*/
    (void)vm;
    (void)data;
    return true;
}

MercuryBool MercuryNoOperation(MercuryVM*  vm,
                               MercuryBool flags,
                               MercuryBool wrap) {
    (void)vm;
    (void)flags;
    (void)wrap;
    return true;
}

MercuryBool MercuryPushAddress(MercuryVM*  vm,
                               MercuryBool flags,
                               MercuryBool wrap) {
    (void)flags;
    #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
    return MercuryPush(vm,
                       vm->CurrentProgram->Data
                       [vm->CurrentProgram->Data
                       [ProgramCounter((wrap)?(0):(1))]]);
    #undef ProgramCounter
}

MercuryBool MercuryPushImmediate(MercuryVM*  vm,
                                 MercuryBool flags,
                                 MercuryBool wrap) {
    (void)flags;
    #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
    return MercuryPush(vm,
                       vm->CurrentProgram->Data
                       [ProgramCounter((wrap)?(0):(1))]);
    #undef ProgramCounter
}

MercuryBool MercuryPopOp(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        vm->CurrentProgram->Data
        [vm->CurrentProgram->Data
        [ProgramCounter((wrap)?(0):(1))]] = MercuryPop(vm);
        #undef ProgramCounter
    }
    else {
        vm->CurrentProgram->Data[MercuryPop(vm)] = MercuryPop(vm);
    }
    return true;
}

MercuryBool MercuryPickOp(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPick(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))]);
        #undef ProgramCounter
    }
    else {
        return MercuryPick(vm, MercuryPop(vm));
    }
}

MercuryBool MercuryRollOp(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    /*Not done!*/
    (void)vm;
    (void)flags;
    (void)wrap;
    return true;
}

MercuryBool MercuryDuplicate(MercuryVM*  vm,
                             MercuryBool flags,
                             MercuryBool wrap) {
    (void)flags;
    (void)wrap;
    return MercuryPick(vm, 0);
}

MercuryBool MercuryDrop(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    (void)flags;
    (void)wrap;
    MercuryPop(vm);
    return true;
}

MercuryBool MercurySwap(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    /*
        --REPLACE--
        This function has been marked for replacement.
        Reason: MercuryRoll is the primitive for this operation.
                It should be used in its implementation.
    */
    MercuryUnsigned operand1;
    MercuryUnsigned operand2;
    (void)flags;
    (void)wrap;
    operand1 = MercuryPop(vm);
    operand2 = MercuryPop(vm);
    if(MercuryPush(vm, operand1) == false ||
       MercuryPush(vm, operand2) == false) {
        return false;
    }
    else {
        return true;
    }
}

MercuryBool MercuryRotate(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    /*Not done!*/
    (void)vm;
    (void)flags;
    (void)wrap;
    return true;
}

MercuryBool MercuryAdd(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [vm->CurrentProgram->ProgramCounter
                                + ((wrap)?(0):(1))] + MercuryPop(vm));
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) + MercuryPop(vm));
    }
}

MercuryBool MercuryIncrement(MercuryVM*  vm,
                             MercuryBool flags,
                             MercuryBool wrap) {
    (void)flags;
    (void)wrap;
    return MercuryPush(vm, MercuryPop(vm) + 1);
}

MercuryBool MercurySubtract(MercuryVM*  vm,
                            MercuryBool flags,
                            MercuryBool wrap) {
    MercurySigned operand1;
    MercurySigned operand2;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        operand1 = vm->CurrentProgram->Data
                   [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
        operand2 = MercuryPop(vm);
    }
    else {
        operand1 = MercuryPop(vm);
        operand2 = MercuryPop(vm);
    }
    return MercuryPush(vm, operand2 - operand1);
}

MercuryBool MercuryDecrement(MercuryVM*  vm,
                             MercuryBool flags,
                             MercuryBool wrap) {
    (void)flags;
    (void)wrap;
    return MercuryPush(vm, MercuryPop(vm) - 1);
}

MercuryBool MercuryMultiply(MercuryVM*  vm,
                            MercuryBool flags,
                            MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [vm->CurrentProgram->ProgramCounter
                                + ((wrap)?(0):(1))] * MercuryPop(vm));
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) * MercuryPop(vm));
    }
}

MercuryBool MercuryDivide(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercurySigned operand1;
    MercurySigned operand2;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        operand1 = vm->CurrentProgram->Data
                   [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
        operand2 = MercuryPop(vm);
    }
    else {
        operand1 = MercuryPop(vm);
        operand2 = MercuryPop(vm);
    }
    if(operand1 == 0) {
        vm->Status  = MercuryDivisionByZero;
        return false;
    }
    else {
        return MercuryPush(vm, operand2 / operand1);
    }
}

MercuryBool MercuryModulo(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercurySigned operand1;
    MercurySigned operand2;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        operand1 = vm->CurrentProgram->Data
                   [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
        operand2 = MercuryPop(vm);
    }
    else {
        operand1 = MercuryPop(vm);
        operand2 = MercuryPop(vm);
    }
    if(operand1 == 0) {
        vm->Status  = MercuryDivisionByZero;
        return false;
    }
    else {
        return MercuryPush(vm, operand2 % operand1);
    }
}

MercuryBool MercuryLogicalAND(MercuryVM*  vm,
                              MercuryBool flags,
                              MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] && MercuryPop(vm));
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) && MercuryPop(vm));
    }
}

MercuryBool MercuryLogicalOR(MercuryVM*  vm,
                             MercuryBool flags,
                             MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] || MercuryPop(vm));
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) && MercuryPop(vm));
    }
}

MercuryBool MercuryLogicalNOT(MercuryVM*  vm,
                              MercuryBool flags,
                              MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           !vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))]);
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, !MercuryPop(vm));
    }
}

MercuryBool MercuryLogicalXOR(MercuryVM*  vm,
                              MercuryBool flags,
                              MercuryBool wrap) {
    MercurySigned operand;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        operand = MercuryPop(vm);
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] != operand);
        #undef ProgramCounter
    }
    else {
        operand = MercuryPop(vm);
        return MercuryPush(vm, operand != MercuryPop(vm));
    }
}

MercuryBool MercuryBitwiseAND(MercuryVM*  vm,
                              MercuryBool flags,
                              MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] & MercuryPop(vm));
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) & MercuryPop(vm));
    }
}

MercuryBool MercuryBitwiseOR(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] | MercuryPop(vm));
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) | MercuryPop(vm));
    }
}

MercuryBool MercuryBitwiseNOT(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           ~vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))]);
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, ~MercuryPop(vm));
    }
}

MercuryBool MercuryBitwiseXOR(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] ^ MercuryPop(vm));
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) ^ MercuryPop(vm));
    }
}

MercuryBool MercuryShiftLeft(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] << MercuryPop(vm));
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) << MercuryPop(vm));
    }
}

MercuryBool MercuryShiftRight(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    if((flags & MercuryImmediate) == MercuryImmediate) {
        #define ProgramCounter(x) (vm->CurrentProgram->ProgramCounter + (x))
        return MercuryPush(vm,
                           vm->CurrentProgram->Data
                           [ProgramCounter((wrap)?(0):(1))] >> MercuryPop(vm));
        #undef ProgramCounter
    }
    else {
        return MercuryPush(vm, MercuryPop(vm) >> MercuryPop(vm));
    }
}

MercuryBool MercuryCompare(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercurySigned operand1;
    MercurySigned operand2;
    MercuryUnsigned result;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        operand1 = vm->CurrentProgram->Data
                   [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
        operand2 = MercuryPop(vm);
    }
    else {
        operand1 = MercuryPop(vm);
        operand2 = MercuryPop(vm);
    }
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(operand2 <  operand1) {
        result = 1;
    }
    else if(operand2 == operand1) {
        result = 2;
    }
    else if(operand2 >  operand1) {
        result = 3;
    }
    else {
        result = 0;
    }
    return MercuryPush(vm, result);
}

MercuryBool MercuryBranchLessThan(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    MercuryUnsigned result;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        address = vm->CurrentProgram->Data
                  [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        address = MercuryPop(vm);
    }
    result = MercuryPop(vm);
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else {
        if(result == 1) {
            vm->CurrentProgram->ProgramCounter = address;
            return true;
        }
        else {
            return false;
        }
    }
}

MercuryBool MercuryBranchLessThanOrEqualTo(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    MercuryUnsigned result;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        address = vm->CurrentProgram->Data
                  [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        address = MercuryPop(vm);
    }
    result = MercuryPop(vm);
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else {
        if(result == 1 ||
           result == 2) {
            vm->CurrentProgram->ProgramCounter = address;
            return true;
        }
        else {
            return false;
        }
    }
}

MercuryBool MercuryBranchEqualTo(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    MercuryUnsigned result;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        address = vm->CurrentProgram->Data
                  [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        address = MercuryPop(vm);
    }
    result = MercuryPop(vm);
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else {
        if(result == 2) {
            vm->CurrentProgram->ProgramCounter = address;
            return true;
        }
        else {
            return false;
        }
    }
}

MercuryBool MercuryBranchGreaterThan(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    MercuryUnsigned result;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        address = vm->CurrentProgram->Data
                  [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        address = MercuryPop(vm);
    }
    result = MercuryPop(vm);
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else {
        if(result == 3) {
            vm->CurrentProgram->ProgramCounter = address;
            return true;
        }
        else {
            return false;
        }
    }
}

MercuryBool MercuryBranchGreaterThanOrEqualTo(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    MercuryUnsigned result;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        address = vm->CurrentProgram->Data
                  [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        address = MercuryPop(vm);
    }
    result = MercuryPop(vm);
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else {
        if(result == 2 ||
           result == 3) {
            vm->CurrentProgram->ProgramCounter = address;
            return true;
        }
        else {
            return false;
        }
    }
}

MercuryBool MercuryJump(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    if((flags & MercuryImmediate) == MercuryImmediate) {
        address = vm->CurrentProgram->Data
                  [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        address = MercuryPop(vm);
    }
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else {
        vm->CurrentProgram->ProgramCounter = address;
        return true;
    }
}

MercuryBool MercuryJumpSubroutine(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    if((flags & (1)) == MercuryImmediate) {
        address = vm->CurrentProgram->Data
                  [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        address = MercuryPop(vm);
    }
    if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else if(MercuryPush(vm,
                        vm->CurrentProgram->ProgramCounter +
                        (((flags & (1)) == MercuryImmediate) ?
                                           (2):(1))) == false) {
            return false;
    }
    else {
        vm->CurrentProgram->ProgramCounter = address;
        return true;
    }
}

MercuryBool MercuryReturnSubroutine(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned address;
    (void)flags;
    (void)wrap;
    address = MercuryPop(vm);
    if(vm->Status != MercuryNone) {
        return false;
    }
    else if(address >= vm->CurrentProgram->Size) {
        vm->Status  = MercuryBranchOutOfRange;
        return false;
    }
    else {
        vm->CurrentProgram->ProgramCounter = address;
        return true;
    }
}

MercuryBool MercuryCallModule(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    MercuryUnsigned module;
    if((flags & (1)) == MercuryImmediate) {
        module = vm->CurrentProgram->Data
                 [vm->CurrentProgram->ProgramCounter + ((wrap)?(0):(1))];
    }
    else {
        module = MercuryPop(vm);
    }
    if(vm->Status != MercuryNone) {
        return false;
    }
    else {
        if(module                    >= vm->ModuleCount ||
           vm->Modules[module]       == MercuryNull     ||
           vm->Modules[module]->Call == MercuryNull) {
            MercuryPush(vm, MercuryMaxValue);
            if(vm->CurrentProgram->ProgramCounter
                                   == vm->CurrentProgram->Size - 2 ||
               vm->CurrentProgram->ProgramCounter
                                   == vm->CurrentProgram->Size - 1) {
                vm->CurrentProgram->ProgramCounter = 0;
            }
            else {
                vm->CurrentProgram->ProgramCounter += (wrap)?(1):(2);
            }
            return true;
        }
        else {
            vm->Modules[module]->Call(vm, vm->Modules[module], wrap);
            return MercuryPush(vm, 0);
        }
    }
}

MercuryBool MercuryHalt(MercuryVM* vm, MercuryBool flags, MercuryBool wrap) {
    (void)flags;
    (void)wrap;
    vm->Running = false;
    return true;
}

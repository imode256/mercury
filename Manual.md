# Mercury OpCode Reference ###
This is a full listing of Mercury's OpCodes.

##No Operation, 0x00##
    No Operation.
    Increments the program counter by 1.
##Push (Address), 0x01##
    Pushes from an address specified by the operand.
    Increments the program counter by 2.
##Push (Immediate), 0x02##
    Pushes the included operand.
    Increments the program counter by 2.
##Pop, 0x03##
    Pops to the address at the top of the stack.
    Increments the program counter by 1.
##Pop (Immediate), 0x04##
    Pops to the address specified by the operand.
    Increments the program counter by 2.
##Pick, 0x05##
    Picks a specific element on the stack and duplicates it to the front.
    Increments the program counter by 1.
##Pick (Immediate), 0x06##
    Picks a specific element on the stack and duplicates it to the front.
    Increments the program counter by 2.
##Roll, 0x07##
    Rolls the entire stack by a number of elements, wrapping the end around to the front.
    Increments the program counter by 1.
##Roll (Immediate), 0x08##
    Rolls the entire stack by a number of elements, wrapping the end around to the front.
    Increments the program counter by 2.
##Duplicate, 0x09##
    Duplicates the top of the stack.
    Increments the program counter by 1.
##Drop, 0x0A##
    Deletes the top of the stack.
    Increments the program counter by 1.
##Swap, 0x0B##
    Swaps two elements on the top of the stack.
    Increments the program counter by 1.
##Rotate, 0x0C##
    Rotates the top three stack items.
    Increments the program counter by 1.
##Add, 0x0D##
    Adds the top two stack items.
    Increments the program counter by 1.
##Add (Immediate), 0x0E##
    Adds the top of the stack with the operand.
    Increments the program counter by 2.
##Increment, 0x0F##
    Increments the top of the stack by 1.
    Increments the program counter by 1.
##Subtract, 0x10##
    Subtracts the top two stack items.
    Increments the program counter by 1.
##Subtract (Immediate), 0x11##
    Subtracts the top of the stack with the operand.
    Increments the program counter by 2.
##Decrement, 0x12##
    Decrements the top of the stack by 1.
    Increments the program counter by 1.
##Multiply, 0x13##
    Multiplies the top two stack items.
    Increments the program counter by 1.
##Multiply (Immediate), 0x14##
    Multiplies the top of the stack by the operand.
    Increments the program counter by 2.
##Divide, 0x15##
    Divides the top two stack items.
    Increments the program counter by 1.
##Divide (Immediate), 0x16##
    Divides the top of the stack by the operand.
    Increments the program counter by 2.
##Modulo, 0x17##
    Performs modular arithmetic with the top two stack items.
    Increments the program counter by 1.
##Modulo (Immediate), 0x18##
    Performs modular arithmetic with the top of the stack and the operand.
    Increments the program counter by 2.
##Logical AND, 0x19##
    Performs a logical AND with the top two stack items.
    Increments the program counter by 1.
##Logical AND (Immediate), 0x1A##
    Performs a logical AND with the top of the stack and the operand.
    Increments the program counter by 2.
##Logical OR, 0x1B##
    Performs a logical OR with the top two stack items.
    Increments the program counter by 1.
##Logical OR (Immediate), 0x1C##
    Performs a logical OR with the top of the stack and the operand.
    Increments the program counter by 2.
##Logical NOT, 0x1D##
    Performs a logical NOT with the top of the stack.
    Increments the program counter by 1.
##Logical NOT (Immediate), 0x1E##
    Performs a logical NOT with the operand and pushes it to the stack.
    Increments the program counter by 2.
##Logical XOR, 0x1F##
    Performs a logical XOR with the top two stack items.
    Increments the program counter by 1.
##Logical XOR (Immediate), 0x20##
    Performs a logical XOR with the top of the stack and the operand.
    Increments the program counter by 2.
##Bitwise AND, 0x21##
    Performs a bitwise AND with the top two stack items.
    Increments the program counter by 1.
##Bitwise AND (Immediate), 0x22##
    Performs a bitwise AND with the top of the stack and the operand.
    Increments the program counter by 2.
##Bitwise OR, 0x23##
    Performs a bitwise OR with the top two stack items.
    Increments the program counter by 1.
##Bitwise OR (Immediate), 0x24##
    Performs a bitwise OR with the top of the stack and the operand.
    Increments the program counter by 2.
##Bitwise NOT, 0x25##
    Performs a bitwise NOT with the top of the stack.
    Increments the program counter by 1.
##Bitwise NOT (Immediate), 0x26##
    Performs a bitwise NOT with the operand and pushes it to the stack.
    Increments the program counter by 2.
##Bitwise XOR, 0x27##
    Performs a bitwise XOR with top two stack items.
    Increments the program counter by 1.
##Bitwise XOR (Immediate), 0x28##
    Performs a bitwise XOR with the top of the stack and the operand.
    Increments the program counter by 2.
##Shift Left, 0x29##
    Performs a bitwise shift left with top two stack items.
    Increments the program counter by 1.
##Shift Left (Immediate), 0x2A##
    Performs a bitwise shift left with the top of the stack and the operand.
    Increments the program counter by 2.
##Shift Right, 0x2B##
    Performs a bitwise shift right with top two stack items.
    Increments the program counter by 1.
##Shift Right (Immediate), 0x2C##
    Performs a bitwise shift right with the top of the stack and the operand.
    Increments the program counter by 2.
##Compare, 0x2D##
    Compares the top two stack items.
    Increments the program counter by 1.
##Compare (Immediate), 0x2E##
    Compares the top of the stack with the operand.
    Increments the program counter by 2.
##Branch Less Than, 0x2F##
    Branches to the second item on the stack if the first's condition code is 1.
    Increments the program counter by 1.
##Branch Less Than (Immediate), 0x30##
    Branches to the operand if the top of the stack's condition code is 1.
    Increments the program counter by 2.
##Branch Less Than or Equal to, 0x31##
    Branches to the second item on the stack if the first's condition code is 1 or 2.
    Increments the program counter by 1.
##Branch Less Than or Equal to (Immediate), 0x32##
    Branches to the operand if the top of the stack's condition code is 1 or 2.
    Increments the program counter by 2.
##Branch Equal to, 0x33##
    Branches to the second item on the stack if the first's condition code is 2.
    Increments the program counter by 1.
##Branch Equal to (Immediate), 0x34##
    Branches to the operand if the top of the stack's condition code is 2.
    Increments the program counter by 2.
##Branch Greater Than, 0x35##
    Branches to the second item on the stack if the first's condition code is 3.
    Increments the program counter by 1.
##Branch Greater Than (Immediate), 0x36##
    Branches to the operand if the top of the stack's condition code is 3.
    Increments the program counter by 2.
##Branch Greater Than or Equal to, 0x37##
    Branches to the second item on the stack if the first's condition code is 2 or 3.
    Increments the program counter by 1.
##Branch Greater Than or Equal to (Immediate), 0x38##
    Branches to the operand if the top of the stack's condition code is 2 or 3.
    Increments the program counter by 2.
##Jump, 0x39##
    Jumps to the top of the stack.
    Increments the program counter by 1.
##Jump (Immediate), 0x3A##
    Jumps to the operand.
    Increments the program counter by 2.
##Jump Subroutine, 0x3B##
    Performs a jump to a subroutine specified by the top of the stack.
    Increments the program counter by 1.
##Jump Subroutine (Immediate), 0x3C##
    Performs a jump to a subroutine specified by the operand.
    Increments the program counter by 2.
##Return Subroutine, 0x3D##
    Returns from a subroutine.
    Does not increment the program counter.
##Call Module, 0x3E##
    Calls a module specified by the top of the stack.
    Increments the program counter by 1.
##Call Module (Immediate), 0x3F##
    Calls a module specified by the operand.
    Increments the program counter by 2.
##Halt, 0x40
    Halts the machine.
    Increments the program counter by 1.

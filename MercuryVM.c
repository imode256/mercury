#include "MercuryVM.h"

MercuryData         (*MercuryAllocate)(MercurySize) = MercuryNull;
void                (*MercuryFree)(MercuryData)     = MercuryNull;
const MercurySize   MercuryMaxSize                  = (unsigned long)
                                                      ~((unsigned long)0);
const unsigned char MercuryCompiledWordSize         = sizeof(MercuryUnsigned);

MercuryBool MercuryInitialize(MercuryData (*allocate)(MercurySize),
                              void        (*free)(MercuryData)) {
    if(allocate == MercuryNull ||
       free     == MercuryNull) {
        return false;
    }
    else {
        MercuryAllocate = allocate;
        MercuryFree     = free;
        return true;
    }
}

MercuryVM* MercuryCreateVM(MercurySize stacksize, MercurySize modulecount) {
    MercuryVM*  vm;
    MercurySize iterator;
    if(MercuryAllocate == MercuryNull ||
       MercuryFree     == MercuryNull) {
        return MercuryNull;
    }
    else {
        if(MercuryMaxValue < stacksize - 1        ||
           MercuryMaxValue < modulecount - 1      ||
           MercuryValidSize(stacksize) == false   ||
           MercuryValidSize(modulecount) == false) {
            return MercuryNull;
        }
        vm = MercuryAllocate((sizeof(MercuryVM)) +
                             (sizeof(MercurySigned) * stacksize) +
                             (sizeof(MercuryModule*) * modulecount));
        if(vm == MercuryNull) {
            return MercuryNull;
        }
        vm->CurrentProgram = MercuryNull;
        vm->Stack          = (MercurySigned*)(vm + 1);
        vm->Modules        = (MercuryModule**)(vm->Stack + stacksize);
        for(iterator = 0; iterator < stacksize; iterator++) {
            vm->Stack[iterator] = 0;
        }
        for(iterator = 0; iterator < modulecount; iterator++) {
            vm->Modules[iterator] = MercuryNull;
        }
        vm->StackSize      = stacksize;
        vm->ModuleCount    = modulecount;
        vm->StackTop       = 0;
        vm->Status         = 0;
        vm->Running        = true;
        return vm;
    }
}

MercuryBool MercuryCreateModule(MercuryVM*  vm,
                                MercuryBool (*init)(MercuryVM*, MercuryModule*),
                                MercuryBool (*call)(MercuryVM*,
                                                    MercuryModule*,
                                                    MercuryBool),
                                MercuryBool (*update)(MercuryVM*,
                                                      MercuryModule*,
                                                      MercuryBool),
                                MercuryBool (*shutdown)(MercuryVM*,
                                                        MercuryModule*),
                                MercuryData data,
                                MercurySize slot) {
    if(vm                == MercuryNull     ||
       slot              >= vm->ModuleCount ||
       vm->Modules[slot] != MercuryNull) {
        return false;
    }
    else {
        vm->Modules[slot] = MercuryAllocate(sizeof(MercuryModule));
        if(vm->Modules[slot] == MercuryNull) {
            return false;
        }
        vm->Modules[slot]->Init     = init;
        vm->Modules[slot]->Call     = call;
        vm->Modules[slot]->Update   = update;
        vm->Modules[slot]->Shutdown = shutdown;
        vm->Modules[slot]->Slot     = slot;
        vm->Modules[slot]->Data     = data;
        return true;
    }
}

MercuryBool MercuryAddModule(MercuryVM*     vm,
                             MercuryModule* module,
                             MercurySize    slot) {
    if(vm                == MercuryNull     ||
       module            == MercuryNull     ||
       slot              >= vm->ModuleCount ||
       vm->Modules[slot] != MercuryNull) {
        return false;
    }
    else {
        vm->Modules[slot] = module;
        return true;
    }
}

MercuryBool MercuryDeleteModule(MercuryVM* vm, MercurySize slot) {
    if(vm                == MercuryNull     ||
       slot              >= vm->ModuleCount ||
       vm->Modules[slot] == MercuryNull) {
        return false;
    }
    else {
        MercuryFree(vm->Modules[slot]);
        vm->Modules[slot] = MercuryNull;
        return true;
    }
}

MercuryVM* MercuryResizeVM(MercuryVM*  vm,
                           MercurySize stacksize,
                           MercurySize modulecount) {
    MercuryVM*  result;
    MercurySize iterator;
    if(vm          == MercuryNull       ||
       (stacksize  == vm->StackSize  &&
       modulecount == vm->ModuleCount)  ||
       stacksize   == 0                 ||
       modulecount == 0) {
        return MercuryNull;
    }
    else {
        result = MercuryCreateVM(stacksize, modulecount);
        if(result == MercuryNull) {
            return MercuryNull;
        }
        #define Min(x, y) ((x < y) ? (x) : (y))
        for(iterator = 0;
            iterator < Min(vm->StackSize, stacksize);
            iterator++) {
            ((unsigned char*)result->Stack)[iterator] = 
            ((unsigned char*)vm->Stack)[iterator];
        }
        for(iterator = 0;
            iterator < Min(vm->ModuleCount, modulecount);
            iterator++) {
            result->Modules[iterator] = vm->Modules[iterator];
        }
        #undef Min
        result->CurrentProgram = vm->CurrentProgram;
        result->StackTop       = vm->StackTop;
        result->Status         = vm->Status;
        result->Running        = vm->Running;
        MercuryFree(vm);
        return result;
    }
}

MercuryBool MercurySetCurrentProgram(MercuryVM* vm, MercuryProgram* program) {
    if(vm      == MercuryNull ||
       program == MercuryNull) {
        return false;
    }
    else {
        vm->CurrentProgram = program;
        return true;
    }
}

MercuryBool MercuryExecuteOperation(MercuryVM* vm, MercuryOperation* op) {
    MercuryBool result;
    MercuryBool wrap;
    wrap = false;
    if(vm          == MercuryNull ||
       vm->Running == false) {
        return false;
    }
    else {
        if(vm->CurrentProgram->ProgramCounter == vm->CurrentProgram->Size - 1) {
            vm->CurrentProgram->ProgramCounter = 0;
            wrap                               = true;
        }
        result = op->Function(vm, op->Flags, wrap);
        if(result && vm->Running == true) {
            if((op->Flags
                & MercuryImmediate)            == MercuryImmediate            &&
               (op->Flags
                & MercuryModifyProgramCounter) != MercuryModifyProgramCounter &&
               (op->Flags
                & MercuryBranch)               != MercuryBranch) {
                if(vm->CurrentProgram->ProgramCounter == 
                       vm->CurrentProgram->Size - 2 ||
                   vm->CurrentProgram->ProgramCounter ==
                       vm->CurrentProgram->Size - 1) {
                    vm->CurrentProgram->ProgramCounter = 0;
                }
                else {
                    vm->CurrentProgram->ProgramCounter += (wrap)?(1):(2);
                }
                return true;
            }
            else if((op->Flags
                     & MercuryModifyProgramCounter)
                                           == MercuryModifyProgramCounter ||
                    (op->Flags
                     & MercuryBranch)
                                           == MercuryBranch) {
                return true;
            }
            else {
                if(vm->CurrentProgram->ProgramCounter
                                              == vm->CurrentProgram->Size) {
                    vm->CurrentProgram->ProgramCounter = 0;
                }
                else {
                    vm->CurrentProgram->ProgramCounter += (wrap)?(0):(1);
                }
                return true;
            }
        }
        else if(vm->Running == false) {
            return false;
        }
        else {
            if((op->Flags & MercuryBranch) == MercuryBranch) {
                if((op->Flags & MercuryImmediate) == MercuryImmediate) {
                    if(vm->CurrentProgram->ProgramCounter ==
                           vm->CurrentProgram->Size - 2 ||
                       vm->CurrentProgram->ProgramCounter ==
                           vm->CurrentProgram->Size - 1) {
                        vm->CurrentProgram->ProgramCounter = 0;
                    }
                    else {
                        vm->CurrentProgram->ProgramCounter += (wrap)?(1):(2);
                    }
                    return true;
                }
                else {
                    if(vm->CurrentProgram->ProgramCounter ==
                           vm->CurrentProgram->Size) {
                        vm->CurrentProgram->ProgramCounter = 0;
                    }
                    else {
                        vm->CurrentProgram->ProgramCounter += (wrap)?(0):(1);
                    }
                    return true;
                }
            }
            else {
                return false;
            }
        }
    }
}

MercuryBool MercuryRunVM(MercuryVM* vm) {
    MercurySize iterator;
    MercuryBool result;
    result = false;
    if(vm                 == MercuryNull ||
       vm->CurrentProgram == MercuryNull ||
       vm->Running        == false) {
        return false;
    }
    else {
        for(iterator = 0; iterator < vm->ModuleCount; iterator++) {
            if(vm->Modules[iterator]       != MercuryNull &&
               vm->Modules[iterator]->Init != MercuryNull) {
                result = vm->Modules[iterator]->Init(vm, vm->Modules[iterator]);
            }
        }
        #define Operation  (&MercuryOperations       \
                           [vm->CurrentProgram->Data \
                           [vm->CurrentProgram->ProgramCounter]])
        #define MemoryAtPC ((MercuryUnsigned)vm->CurrentProgram->Data \
                           [vm->CurrentProgram->ProgramCounter])
        while(vm->Running == true) {
            if((MercuryUnsigned)MemoryAtPC > sizeof(MercuryOperations)
                                             / sizeof(MercuryOperation)) {
                vm->Running = false;
                return result;
            }
            result = MercuryExecuteOperation(vm, Operation);
        }
        return result;
        #undef MemoryAtPC
        #undef Operation
    }
}

MercuryBool MercuryDeleteVM(MercuryVM* vm) {
    MercurySize iterator;
    if(vm == MercuryNull) {
        return false;
    }
    else if(MercuryAllocate == MercuryNull ||
            MercuryFree     == MercuryNull) {
        return false;
    }
    else {
        for(iterator = 0; iterator < vm->ModuleCount; iterator++) {
            if(vm->Modules[iterator]           != MercuryNull &&
               vm->Modules[iterator]->Shutdown != MercuryNull) {
                vm->Modules[iterator]->Shutdown(vm, vm->Modules[iterator]);
            }
            MercuryFree(vm->Modules[iterator]);
        }
        MercuryDeleteProgram(vm->CurrentProgram);
        MercuryFree(vm);
        return true;
    }
}

#ifndef MercuryOpsIncluded
#define MercuryOpsIncluded
#include "MercuryProgram.h"

/* Fix the layout here. */

MercuryBool   MercuryPush(
                     MercuryVM*      vm,
                     MercuryUnsigned data);
MercurySigned MercuryPop(MercuryVM* vm);
MercuryBool   MercuryPick(
                     MercuryVM*      vm,
                     MercuryUnsigned data);
MercuryBool   MercuryRoll(
                     MercuryVM*      vm,
                     MercuryUnsigned data);
MercuryBool   MercuryNoOperation(
                       MercuryVM*  vm,
                       MercuryBool flags,
                       MercuryBool wrap);
MercuryBool   MercuryPushAddress(
                         MercuryVM*  vm,
                         MercuryBool flags,
                         MercuryBool wrap);
MercuryBool   MercuryPushImmediate(
                         MercuryVM*  vm,
                         MercuryBool flags,
                         MercuryBool wrap);
MercuryBool   MercuryPopOp(
                        MercuryVM*  vm,
                        MercuryBool flags,
                        MercuryBool wrap);
MercuryBool   MercuryPickOp(
                         MercuryVM*  vm,
                         MercuryBool flags,
                         MercuryBool wrap);
MercuryBool   MercuryRollOp(
                         MercuryVM*  vm,
                         MercuryBool flags,
                         MercuryBool wrap);
MercuryBool   MercuryDuplicate(
                     MercuryVM*  vm,
                     MercuryBool flags,
                     MercuryBool wrap);
MercuryBool   MercuryDrop(
                     MercuryVM*  vm,
                     MercuryBool flags,
                     MercuryBool wrap);
MercuryBool   MercurySwap                      (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryRotate                    (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryAdd                       (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryIncrement                 (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercurySubtract                  (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryDecrement                 (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryMultiply                  (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryDivide                    (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryModulo                    (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryLogicalAND                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryLogicalOR                 (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryLogicalNOT                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryLogicalXOR                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBitwiseAND                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBitwiseOR                 (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBitwiseNOT                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBitwiseXOR                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryShiftLeft                 (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryShiftRight                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryCompare                   (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBranchLessThan            (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBranchLessThanOrEqualTo   (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBranchEqualTo             (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBranchGreaterThan         (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryBranchGreaterThanOrEqualTo(MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryJump                      (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryJumpSubroutine            (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryReturnSubroutine          (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryCallModule                (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);
MercuryBool   MercuryHalt                      (MercuryVM* vm, MercuryBool flags, MercuryBool wrap);

#endif

#ifndef MercuryGeneralIncluded
#define MercuryGeneralIncluded

#define MercuryNull                 ((void*)0)
#define MercuryNone                 0x00
#define MercuryImmediate            0x01
#define MercuryModifyProgramCounter 0x02
#define MercuryBranch               0x04

#define MercuryStackUnderflow       0x01
#define MercuryStackOverflow        0x02
#define MercuryDivisionByZero       0x03
#define MercuryBranchOutOfRange     0x04

#define MercuryCompatibleWordSize   (MercuryWordSize == MercuryCompiledWordSize)
#define MercuryValidSize(x)         (!(x > MercuryMaxSize \
                                    / sizeof(MercurySigned)))

#if   defined(MercuryChar)
    #define MercuryMaxValue 0xFF
    #define MercuryWordSize 0x01
    #define MercuryWord char
#elif defined(MercuryShort)
    #define MercuryMaxValue 0xFFFF
    #define MercuryWordSize 0x02
    #define MercuryWord short
#elif defined(MercuryInt16)
    #define MercuryMaxValue 0xFFFF
    #define MercuryWordSize 0x02
    #define MercuryWord int
#elif defined(MercuryInt32)
    #define MercuryMaxValue 0xFFFFFFFF
    #define MercuryWordSize 0x04
    #define MercuryWord int
#elif defined(MercuryLong32)
    #define MercuryMaxValue 0xFFFFFFFF
    #define MercuryWordSize 0x08
    #define MercuryWord long
#elif defined(MercuryLong64)
    #define MercuryMaxValue 0xFFFFFFFFFFFFFFFF
    #define MercuryWordSize 0x08
    #define MercuryWord long
#else
    #define MercuryMaxValue 0xFF
    #define MercuryWordSize 0x01
    #define MercuryWord char
#endif

typedef unsigned MercuryWord    MercuryUnsigned;
typedef signed MercuryWord      MercurySigned;
typedef unsigned char           MercuryBool;
typedef void*                   MercuryData;
typedef unsigned long           MercurySize;
typedef struct MercuryOperation MercuryOperation;
typedef struct MercuryModule    MercuryModule;
typedef struct MercuryProgram   MercuryProgram;
typedef struct MercuryVM        MercuryVM;
enum { false, true };
extern const MercurySize        MercuryMaxSize;
extern const unsigned char      MercuryCompiledWordSize;
extern MercuryData              (*MercuryAllocate)(MercurySize);
extern void                     (*MercuryFree)(MercuryData);

struct MercuryOperation {
    MercuryBool (*Function)(MercuryVM*, MercuryBool, MercuryBool);
    MercuryBool Flags;
};

struct MercuryModule {
    MercuryBool (*Init)    (MercuryVM*, MercuryModule*);
    MercuryBool (*Call)    (MercuryVM*, MercuryModule*, MercuryBool);
    MercuryBool (*Update)  (MercuryVM*, MercuryModule*, MercuryBool);
    MercuryBool (*Shutdown)(MercuryVM*, MercuryModule*);
    MercurySize Slot;
    MercuryData Data;
};

struct MercuryProgram {
    MercurySigned* Data;
    MercurySize    Size;
    MercurySize    ProgramCounter;
};

struct MercuryVM {
    MercuryProgram* CurrentProgram;
    MercurySigned*  Stack;
    MercuryModule** Modules;
    MercurySize     StackSize;
    MercurySize     ModuleCount;
    MercurySize     StackTop;
    MercuryUnsigned Status;
    MercuryBool     Running;
};

extern MercuryOperation MercuryOperations[0x41];

#endif

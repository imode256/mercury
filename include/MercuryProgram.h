#ifndef MercuryProgramIncluded
#define MercuryProgramIncluded
#include "MercuryGeneral.h"

MercuryProgram* MercuryCreateProgram(MercurySigned* data, MercurySize size);
MercuryProgram* MercuryResizeProgram(MercuryProgram* program, MercurySize size);
MercuryBool     MercuryDeleteProgram(MercuryProgram* program);

#endif

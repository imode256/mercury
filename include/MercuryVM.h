#ifndef MercuryVMIncluded
#define MercuryVMIncluded
#include "MercuryOps.h"

MercuryBool MercuryInitialize(
                   MercuryData (*allocate)(MercurySize),
                   void        (*free)(MercuryData));
MercuryVM*  MercuryCreateVM(
                         MercurySize stacksize,
                         MercurySize modulecount);
MercuryBool MercuryCreateModule(
                         MercuryVM*  vm,
                         MercuryBool (*init)(MercuryVM*, MercuryModule*),
                         MercuryBool (*call)(MercuryVM*,
                                             MercuryModule*,
                                             MercuryBool),
                         MercuryBool (*update)(MercuryVM*,
                                               MercuryModule*,
                                               MercuryBool),
                         MercuryBool (*shutdown)(MercuryVM*, MercuryModule*),
                         MercuryData data,
                         MercurySize slot);
MercuryBool MercuryAddModule(
                      MercuryVM*     vm,
                      MercuryModule* module,
                      MercurySize    slot);
MercuryBool MercuryDeleteModule     (MercuryVM* vm, MercurySize slot);
MercuryVM*  MercuryResizeVM(
                         MercuryVM*  vm,
                         MercurySize stacksize,
                         MercurySize modulecount);
MercuryBool MercurySetCurrentProgram(MercuryVM* vm, MercuryProgram* program);
MercuryBool MercuryExecuteOperation (MercuryVM* vm, MercuryOperation* op);
MercuryBool MercuryRunVM            (MercuryVM* vm);
MercuryBool MercuryDeleteVM         (MercuryVM* vm);

#endif

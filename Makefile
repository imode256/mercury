.SILENT:static
.SILENT:shared
.SILENT:clean

compiler = clang
archiver = ar
inc = ./include
lib = ./lib
library = Mercury
wordsize = MercuryChar
objects = $(library)VM.o $(library)Ops.o $(library)Program.o
sources = $(library)VM.c $(library)Ops.c $(library)Program.c
libflags = -O0 -g -c -I$(inc) -std=c89 -Wall -Wextra -Werror -pedantic -nostdlib -D$(wordsize)
arflags = rcs

static: clean
	mkdir $(lib)
	$(compiler) $(libflags) $(sources)
	$(archiver) $(arflags) $(lib)/lib$(library).a $(objects)
	rm -rf $(objects)
	echo "Compilation successful. Static library created."
shared: clean
	mkdir $(lib)
	$(compiler) -fpic $(libflags) $(sources)
	$(compiler) -shared -o $(lib)/lib$(library).so $(objects)
	rm -rf $(objects)
	echo "Compilation successful. Shared library created."
clean:
	rm -rf $(objects) ./a.out $(lib)
	echo "Mercury Cleaned."

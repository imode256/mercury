#include "MercuryProgram.h"

MercuryProgram* MercuryCreateProgram(MercurySigned* data, MercurySize size) {
    MercuryProgram* program;
    MercurySize     iterator;
    if(data                   == MercuryNull ||
       size                   == 0           ||
       MercuryMaxValue        < size - 1     ||
       MercuryValidSize(size) == false) {
        return MercuryNull;
    }
    else {
        program = MercuryAllocate((sizeof(MercuryProgram)) +
                                  (sizeof(MercurySigned) * size));
        if(program == MercuryNull) {
            return MercuryNull;
        }
        program->Data           = (MercurySigned*)(program + 1);
        for(iterator = 0; iterator < size; iterator++) {
            ((unsigned char*)program->Data)[iterator] = 
            ((unsigned char*)data)[iterator];
        }
        program->Size           = size;
        program->ProgramCounter = 0;
        return program;
    }
}

MercuryProgram* MercuryResizeProgram(MercuryProgram* program,
                                     MercurySize size) {
    if(program == MercuryNull) {
        return MercuryNull;
    }
    else {
        return MercuryCreateProgram(program->Data, size);
    }
}

MercuryBool MercuryDeleteProgram(MercuryProgram* program) {
    if(program == MercuryNull) {
        return false;
    }
    else {
        MercuryFree(program);
        return true;
    }
}
